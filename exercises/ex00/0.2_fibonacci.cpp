#include <iostream>

/*  NOTE 1:
		Takes forever since this is the bottom up way.
		I.e. we calculate a lot of stuff several times.
		Use some dyn. prog. to "fix" it: Swap memory for speed.
*/
unsigned fibonacci_recursive(unsigned n) {
	if (n==0)
		return 0;
	if (n==1)
		return 1;
	return fibonacci_recursive(n-1) + fibonacci_recursive(n-2);
}


/*	NOTE 2:
		Way faster but the problem is that fr n=82
		we get an integer overflow.
		What type would we use now?
*/
unsigned fibonacci_loop(unsigned n) {
	if (n==0)
		return 0;
	if (n==1)
		return 1;

	unsigned old_value = 0;
	unsigned new_value = 1;
	unsigned tmp = 0;
	for(unsigned i=n-1; i!=0; --i) {
		new_value += old_value;
		old_value = new_value-old_value;
	}

	return new_value;
}

int main() {
	unsigned n;

	std::cout << "n=";
	std::cin >> n;
	std::cout << "fibonacci_recursive(" << n << ")=" << fibonacci_recursive(n) << "\n";
	std::cout << "fibonacci_loop(" << n << ")=" << fibonacci_loop(n) << "\n";
	
	return 0;
}