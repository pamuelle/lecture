# Notes for Ex 1

## Windows installation
* From power shell with admin rights:
```
Enable-WindowsOptionalFeature -Online -FeatureName VirtualMachinePlatform
Enable-WindowsOptionalFeature -Online -FeatureName Microsoft-Windows-Subsystem-Linux
```
* Enable WSL2 BEFORE installing linux
`wsl --set-default-version 2`
* Install Ubuntu 18 from Windows Store.
* allow to copy/paste from Properties.

# Useful packages
install with `sudo apt-get install <package>`

* g++ Possibly >= 8 (c++ 17 is cool)
* git
* cmake
* cmake-curses-gui (ccmake)
* gdb
* emacs (or vim for the heretics)

## Merge sort
Test sort at https://leetcode.com/problems/sort-an-array
Compete for glory.

## Further material
* CppCon talks, especially from Bjarne Stroustrup.

