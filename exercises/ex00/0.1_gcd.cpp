#include <iostream>

unsigned gcd_recursive(unsigned a, unsigned b) {
	unsigned t;
	if ( b!=0 )
		gcd_recursive(b, a%b);
	else
		return a;
}

/*  
	QUESTION 1:
		g++ ./01_gcd.cpp gives a warning:
		0.1_gcd.cpp: In function ‘unsigned int gcd_recursive(unsigned int, unsigned int)’:
		0.1_gcd.cpp:9:1: warning: control reaches end of non-void function [-Wreturn-type]
		    9 | }
		      | ^
		Was wondering if I could rewrite it so that this warnings disappears?
*/	

unsigned gcd_loop(unsigned a, unsigned b) {
	unsigned t;
	while( b!=0 ) {
		t = b;
		b = a%b;
		a = t;
	}

	return a;
}

/*
	QUESTION 2:
		Is it okay to use unsigned as a shortcut for unsigned int?
*/

int main() {
	// Allocate memory
	unsigned a,b;

	// Read input
	std::cout << "Provide a>0, b>0 to get gcd(a,b)\n";
	std::cin >> a >> b;
	std::cout << "gcd_loop(a,b)=" << gcd_loop(a,b) << "\n";
	std::cout << "gcd_recursive(a,b)=" << gcd_recursive(a,b) << "\n";

	return 0;
}