#include <iostream>
#include <limits>

int main() {
	// Machine Epsilon is: smallest number s.t. 1.0 + eps != 1.0
	float eps = 1; // initial value isn't important. 
	while( (1 + eps) != 1)
		eps /= 2;

	std::cout << "Calculated epsilon=" << eps*2 << "\n";
	std::cout << "Precise epsilon=" << std::numeric_limits<float>::epsilon() << "\n";

	return 0;
}