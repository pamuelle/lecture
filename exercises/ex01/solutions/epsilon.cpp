/* Programming Techniques for Scientific Simulations, HS 2017
 * Exercise 1.5
 */

#include <iostream>
#include <limits>

/*
By continually dividing a 'trial epsilon' by two, we can
estimate the true epsilon up to a factor of 2 uncertainty.
*/
double machine_epsilon_divide() {

  const double beta(1.0);
  double alpha(1.0);

  while(double(alpha+beta)>beta)
    alpha /= 2;

  return alpha*2;
}

/*
The bisection method gets the true value of epsilon as
defined in the lecture. Note that this is not the same
as std::numeric_limits<double>::epsilon(). The reason
for this is that we define epsilon as the smallest
number such that 1 + epsilon != 1, whereas numeric_limits
uses the definition the definition that the smallest
number larger than 1 is 1 + epsilon. This is different
because in our definition 1 + epsilon doesn't have to
be representible as a double, it just has to be rounded
(up) to a value other than 1.
*/
double machine_epsilon_bisection() {

  double lower(0.);
  double upper(1.);
  const double one(1.0);
  double old_average(0.);
  double average(1.);

  while (old_average != average) {
    old_average = average;
    average = (upper + lower) / 2.;
    if(one + average == one) {
        lower = average;
    }
    else {
        upper = average;
    }
  }
  return upper;
}

int main() {

  std::cout
    << "machine epsilon:" << std::endl
    << "division method: " << machine_epsilon_divide() << std::endl
    << "bisection method: " << machine_epsilon_bisection() << std::endl
    << "numeric_limits value: " << std::numeric_limits<double>::epsilon() << std::endl;

  return 0;
}
