#include <iostream>
#include <cmath>
double f(double x) {
	//return sin(x);
	return x*(1-x);
}

/*	NOTE 1:
		Using formula from https://en.wikipedia.org/wiki/Simpson%27s_rule
		-> int_a^b P(x) dx = frac{b-a}{6}(f(a)+4f(\frac{a+b}{2})+f(b))

	QUESTION 1:
		I knoe that we could pass a c++ function as an argument and thus
		use simpson() for any math. function we want (we just make a c++
		function out of it). What is done usually?
*/
double simpson(double a, double b, int N) {
	double value = 0.0;
	double value_dx = 0.0;
	double delta_x = (b-a)/N;

	for(double x = a; x <= b; x+=delta_x) {
		value += (delta_x/6.0)*( f(x) + 4*f(x+0.5*delta_x) + f(x+delta_x) );
	}

	return value;
}

int main() {
	int N;
	std::cout << "N=";
	std::cin >> N;
	std::cout << simpson(0,1,N);
	return 0;
}