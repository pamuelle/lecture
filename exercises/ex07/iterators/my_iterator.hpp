#ifndef MY_ITERATOR_HPP
#define MY_ITERATOR_HPP
#include <cassert>
#include <iostream> // for debugging
#include <iterator> // for iterator category tags

/*
Forward & bidirectional iterators requirements:

Iterator:
- CopyConstructible
- CopyAssignable
- Destructible
- Supports: *a (Dereferenceable)
- Supports: ++a (Preincrementable)

Input Iterator:
- All requirements of an iterator.
- Supports: == (EqualityComparable)
- Supports: !=
- Supports: ->
- Supports: a++ (Postincrementable)

Forward Iterator:
- All requirements of an input iterator
- DefaultConstructible
- Supports expression: *a++

Bidirectional Iterator:
- All requirements of a forward iterator
- Predecrementable
- Postdecrementable
- Supports expression: *a--

*/

// my iterator
template <typename T>
class MyIterator {
  public:
    // member types
    using value_type = T; // type of values obtained when dereferencing the
                          // iterator
    using difference_type = std::size_t; // signed integer type to represent
                                         // distance between iterators
    using reference = T&; // type of reference to type of values
    using pointer = T*; // type of pointer to the type of values
    using iterator_category = std::forward_iterator_tag; // category of
                                                         // the iterator
    // using iterator_category = std::bidirectional_iterator_tag; // category of
    //                                                            // the iterator

    // TODO: constructor
    MyIterator() = default;
    MyIterator(pointer it) : it(it) {};
    // copy ctor
    MyIterator(MyIterator const&) = default;
    // copy assignment
    MyIterator& operator=(MyIterator const&) = default;
    // dtor
    ~MyIterator() { }

    // TODO: operators
    reference operator*() {
      return *it; // TODO: dereferencing nullptr?
    }

    bool operator==(const MyIterator<T>& other) {
      return  this->it == other.it && other.it == this->it;
    }
    
    bool operator!=(const MyIterator<T>& other) {
      return !(*this==other);
    }

    // Prefix increment
    MyIterator<T>& operator++() {
      ++it;
      return *this;
    }

    // Prefix decrement
    MyIterator<T>& operator--() {
      --it;
      return *this;
    }

  private:
    pointer it = nullptr;
    // TODO: private members
    // TODO: private method "check" to prevent illegal operations
};

#endif /* MY_ITERATOR_HPP */
