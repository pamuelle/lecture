/* Programming Techniques for Scientific Simulations, HS 2019
 * Exercise 7.1
 */

#include <algorithm>
#include <iostream>
#include <iterator>
#include <limits>
#include <utility>
#include <vector>
#include <list>
#include <set>
#include "timer/timer.hpp"

using test_type = unsigned long;

template <class C>
void fill_container(const size_t & num_ops, C & container) {
    typename C::iterator it;

    for( size_t i = 0; i < num_ops; ++i ) {
        it = container.insert(container.begin(), 0);
        container.erase(it);
    }
}

template<typename C>
void measure_container(const size_t & num_ops,
                       const std::vector<test_type> & input) {

    Timer t;

    // C is std::vector<test_type>, std::list<test_type> or std::set<test_type>
    C container(input.cbegin(),input.cend()); // copy the input to our container

    t.start();
    fill_container(num_ops, container);
    t.stop();

    // output in nanoseconds
    std::cout << ' ' << t.duration()/num_ops*1000000000.;
}

int main() {

    const size_t num_ops = 4e06;

    std::cout << "# N Vector[ns/op] List[ns/op] Set[ns/op]" << std::endl;
    for(unsigned i = 4; i < 14; ++i){
        const size_t size = 1ul << i; // == std::pow(2, i)

        std::vector<test_type> input(size);

        for(size_t i=0; i < input.size(); ++i)
            input[i] = i+1;

        std::cout << size;

        //vector
        measure_container<std::vector<test_type>>(num_ops, input);
        //list
        measure_container<std::list<test_type>>(num_ops, input);
        // set
        measure_container<std::set<test_type>>(num_ops, input);
        //----

        std::cout << std::endl;
    }

    return 0;

}
