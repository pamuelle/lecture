#!/bin/bash
# we want to simulate this many timesteps
TIME=10000
M=2
T=2
R=8
NMAX=10000
# start with 10% population
N0=$((NMAX/10))
# this many data points are measured
NMEAS=$TIME

cmake -Bbuild -H./
make -C build penna_sim
echo "Running with TIME=$TIME M=$M T=$T R=$R NMAX=$NMAX, N0=$N0..."
./build/penna_sim $TIME $M $T $R $NMAX $N0 $NMEAS > population.dat
gnuplot population.gnuplot
gnuplot gene_histogram.gnuplot
