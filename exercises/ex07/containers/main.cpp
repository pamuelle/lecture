#include <iostream>
#include <vector>
#include <list>
#include <set>
#include <cmath>

#include "timer.hpp"
#include "matplotlibcpp.h"

namespace plt = matplotlibcpp;

int main() 
{
	// We only do insert and erase at the beginning of the containers ONCE.
	// We should do it like 1million times, but meh.
	size_t sizes[11];
	std::vector<size_t> times;
	std::vector<size_t> times_log;

	std::vector<double> vec_ins_measure;
	std::vector<double> list_ins_measure;
	std::vector<double> set_ins_measure;

	std::vector<double> vec_del_measure;
	std::vector<double> list_del_measure;
	std::vector<double> set_del_measure;

	for(size_t i=0; i<11; ++i) {
		times.push_back( std::pow(2,i+4) ); //2^4,..,2^14
		times_log.push_back( std::log( std::pow(2,i+4)) ); // log(2^4),..,log(2^14)
	}

	for(size_t i=0; i<11; ++i) {
		size_t n = times[i];

		// Fill array
		int arr[n];
		for(size_t i=0; i<n; ++i) {
			arr[i] = i+1;
		}

		// Measure vector
		std::vector<int> vec;
		for(size_t i=0; i<n; ++i) {
			vec.push_back( arr[i] );
		}

		// Measure list
		std::vector<int> lst;
		for(size_t i=0; i<n; ++i) {
			lst.push_back( arr[i] );
		}

		// Measure set
		std::vector<int> st;
		for(size_t i=0; i<n; ++i) {
			st.push_back( arr[i] );
		}

		//
		// Insert 0 at beginning using insert()
		//

		Timer T;

		// Measure vector
		T.start();
		vec.insert(vec.begin(), 0);
		T.stop();
		vec_ins_measure.push_back(  std::log(T.duration())  );

		// Measure list
		T.start();
		lst.insert(lst.begin(), 0);
		T.stop();
		list_ins_measure.push_back(  std::log(T.duration())  );

		// Measure set
		T.start();
		st.insert(st.begin(), 0);
		T.stop();
		set_ins_measure.push_back(  std::log(T.duration())  );

		//
		// Erase the just added zero using erase()
		//

		// Measure vector
		T.start();
		vec.erase( vec.begin() );
		T.stop();
		vec_del_measure.push_back(  std::log(T.duration())  );

		// Measure list
		T.start();
		lst.erase( lst.begin() );
		T.stop();
		list_del_measure.push_back(  std::log(T.duration())  );

		// Measure set
		T.start();
		st.erase( st.begin() );
		T.stop();
		set_del_measure.push_back(  std::log(T.duration())  );
	}

	// Plot data
	plt::figure();
	plt::plot(times_log, vec_ins_measure);
	plt::plot(times_log, list_ins_measure);
	plt::plot(times_log, set_ins_measure);

	plt::figure();
	plt::plot(times_log, vec_del_measure);
	plt::plot(times_log, list_del_measure);
	plt::plot(times_log, set_del_measure);

	plt::show();

	return 0;
}