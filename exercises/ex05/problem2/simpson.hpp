#include<cmath>
#include<iostream>
#include<cassert>

class MyFunc {
public:
	double operator() (double x, double y) {
		// Unit disk
  		return static_cast<double>(x*x+y*y <= 1.);  // don't use implicit conversion
	}
};

/*	NOTE 1:
		Using formula from https://en.wikipedia.org/wiki/Simpson%27s_rule
		-> int_a^b P(x) dx = frac{b-a}{6}(f(a)+4f(\frac{a+b}{2})+f(b))
*/
template<typename F>
double simpson(const F& f, double a, double b, int N) {
	assert(N>=1);
	
	double value = 0.0;
	double delta_x = (b-a)/N;
	

	//std::cout << "\nIntegrating int_" << a << "^" << b
	//		  << "f(x)dx for N=" << N << "\n\n";

	for(double x = a; x <= b; x+=delta_x) {
		value += (delta_x/6.0)*( f(x) + 4*f(x+0.5*delta_x) + f(x+delta_x) );
	}

	return value;
}