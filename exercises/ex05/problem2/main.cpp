/*
 * NOTE: Solved by using the solution to fully get what's going on.
 *		 Used my simpson function thought - not fully templated yet.
 */


#include <iostream>
#include "simpson.hpp"

double unit_disk(const double x, const double y) {
  return static_cast<double>(x*x+y*y <= 1.);  // don't use implicit conversion
}

double phi(const double x, const double y) {
  return exp(-(x*x+y*y));
}

// Function object for support domain.
struct phi_wrapper {
  public:
  phi_wrapper(double R) : R2_(R*R) {}
  double operator()(const double x, const double y) const {
    if(x*x+y*y <= R2_)
      return phi(x,y);
    return 0.;
  }

  private:
  const double R2_;
};

template<typename F2D>
double integrate2d(double a, double b, int Nx,
				   double c, double d, int Ny,
				   const F2D& f2d) {
	auto F = [&](double x) {
		return simpson([&] (double y) { return f2d(x,y); }, c,d,Ny);
	};
	double I = simpson(F,a,b,Nx);
	return I;
}

int main() {
	const double R = 1.7606;
	const unsigned int N = 1024;

	std::cout.precision(15);

	// Calculate pi
	std::cout << integrate2d(-1,1,N,-1,1,N, &unit_disk) << "\n\n";

	// Solve given problem
	auto phiR = phi_wrapper(R);
  	std::cout << integrate2d(-1., 1., N, -1., 1., N, phiR) << std::endl;


	return 0;
}