/*
 * Programming Techniques for Scientific Simulations I
 * HS 2019
 * Exercise 9.2
 */

#include <iostream>
#include <iomanip>
#include <cmath>
#include <cstdint>
#include <chrono>

int main() {
	using element_t = float;
	constexpr int element_size = sizeof (element_t);
	constexpr int size_min = 1 << 3;
	constexpr int size_max = 1 << 23;
	constexpr int step_min = 1 << 1;
	constexpr int step_max = 1 << 12;
	constexpr int runs = 25;

	auto const array = new element_t[size_max];

	std::cout << "size,step,time" << std::endl;
	for (int size = size_min; size <= size_max; size *= 2) {
		for (int step = step_min; step <= std::min(step_max, size); step *= 2) {
			std::int64_t const sweeps = size_max / size * step;
			std::int64_t const ops = sweeps / step * size;

			for (int run = 0; run < runs; ++run) {
				for (int i = 0; i < size; ++i) {
					array[i] = 0;
				}

				auto const start = std::chrono::high_resolution_clock::now();
				for (int sweep = 0; sweep < sweeps; ++sweep) {
					for (int i = 0; i < size; i += step) {
						array[i] += 1;
					}
				}
				auto const end = std::chrono::high_resolution_clock::now();
				std::chrono::duration<double> const time = end - start;
				std::cout << std::setprecision(12)
					<< size * element_size << ','
					<< step * element_size << ','
					<< time.count() / ops  << std::endl;
			}
		}
	}

	delete[] array;
}
