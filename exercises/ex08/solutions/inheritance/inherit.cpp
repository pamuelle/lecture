#include <iostream>

class A {
  public:
    virtual void f() const { std::cout << "A::f "; }
    void g() const { std::cout << "A::g "; } // not virtual!
};

class B: public A {
  public:
    void f() const { std::cout << "B::f "; } // overrides A::f
    void g() { std::cout << "B::g "; }
};

class C: public B {
  public:
    void f() { std::cout << "C::f "; } // doesn't override B::f (not const!)
    void g() const { std::cout << "C::g "; }
};

// Accepts: A, B (directly derived from A) and C (indirectly derived from A
//          through B)
void func(A const& a) {
  a.f();
  a.g();
}

int main() {

  A a;
  B b;
  C c;

  a.f(); // A::f
  a.g(); // A::g

  b.f(); // B::f
  b.g(); // B::g

  c.f(); // C::f
  c.g(); // C::g

  func(a); // A::f A::g
  func(b); // B::f A::g
  func(c); // B::f A::g

  std::cout << std::endl;

}
