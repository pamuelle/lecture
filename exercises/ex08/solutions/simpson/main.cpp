/* Programming Techniques for Scientific Simulations, HS 2019
 * Exercise 8.2
 */

#define _USE_MATH_DEFINES

#include <cmath>
#include <iostream>

#include "simpson.hpp"


struct sin_lambda_x : public Function {
    sin_lambda_x(const argument_type lambda=1.) : lambda_(lambda) {}

    result_type operator()(const argument_type x) const
    {
        return std::sin(lambda_*x);
    }

private:
    const argument_type lambda_;
};


int main() {
    const sin_lambda_x::argument_type lambda = 2.;
    const sin_lambda_x func(lambda);
    
    const double a = 0.;
    const double b = M_PI / lambda;
    const unsigned int bins = 8;
    
    std::cout.precision(15);
    std::cout << "I = " << integrate(a, b, bins, func)
              << "   (exact value: " << 2./lambda << ")" << std::endl;
  
    return 0;
}
