#include <iostream>
#include <cmath>

// base class provides the function interface
struct SimpleFunction {
	SimpleFunction() {}
	virtual double operator()(double) const = 0;
};

double integrate(SimpleFunction const& f, double a, double b,
				 unsigned int N) {
	double dx = (b - a )/N;
	double xi = a;
	double I = 0.5*f(xi);

	for (unsigned int i = 1; i < N; ++i) {
		xi += dx;
		I += f(xi);
	}
	I += 0.5*f(b);

	return I*dx;
}

// derived class (implements the function!)
struct MyFunc1 : SimpleFunction {
	MyFunc1() {}
	double operator()(double x) const {
	return x*std::sin(x); }
};

struct MyFunc2 : SimpleFunction {
	MyFunc2() {}
	double operator()(double x) const {
	return x*std::sin(x*x)*std::cos(1.0/x); }
};

int main() {
	SimpleFunction* f = nullptr;

	int input;
	std::cout << "Typ 0 for function 1. Type 1 for function 2.\n";
	std::cin >> input;
	switch (input)
    {
        case 0: 
            f = new MyFunc1();
            std::cout <<"f=x*sin(x) = " << integrate(*f, 0., 1., 100) << "\n";
            break;
        case 1:
            f = new MyFunc2();
            std::cout <<"f=x*sin(x*x)*cos(1/x) = " << integrate(*f, 0., 1., 100) << "\n";
            break;
        default:
            std::cout << "Unknown input!";
    }
}