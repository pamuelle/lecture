#include <iostream>
#include <limits>

template<typename T>
T calculateEpsilon() {
	// Machine Epsilon is: smallest number s.t. 1.0 + eps != 1.0
	T eps = 1; // initial value isn't important. 
	while( (1 + eps) != 1)
		eps /= 2;

	return eps*2;
}

int main() {

	std::cout << "Calculated epsilon<float>=" << calculateEpsilon<float>() << "\n";
	std::cout << "Precise epsilon<float>=" << std::numeric_limits<float>::epsilon() << "\n";

	return 0;
}