#include <cstdint>
#include <iostream>
#include <stdexcept>
#include <vector>

#include "fibonacci.hpp"

int main() {
  std::vector<uint32_t> input{0, 1, 2, 3, 42};
  std::vector<uint64_t> expected{0, 1, 1, 2, 267914296};

  for (std::size_t i = 0; i < input.size(); ++i) {
    uint64_t result = fibonacci(input[i]);

    if (result != expected[i]) {
      std::cout << "fibonacci(" << input[i] << ") = " << result
                << "\t(expected: " << expected[i] << ")" << std::endl;

      throw std::logic_error("Test failed!");
    }
  }

  return 0;
}
