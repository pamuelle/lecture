#include <cmath>
#include <iostream>

class Generator {
	public:
		Generator(unsigned int seed) {
			x = seed;
		}

		unsigned int generate() {
			x = (a*x + c)%m;
			return x;
		};

		unsigned int max() {
			return m;
		};
	private:
		unsigned int x = 123;
		unsigned int m = std::pow(2,32)-1;
		unsigned int a = 164525;
		unsigned int c = 1013904223;
};