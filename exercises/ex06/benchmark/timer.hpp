#include <chrono>
#include <iostream>

class Timer {
	public:
		void start() {
			running = true;
			started = true;
			begin = std::chrono::system_clock::now();
		};
		void stop() {
			end = std::chrono::system_clock::now();
			running = false;
		};

		// Returns duration in seconds
		double duration() {
			// No Measurement ever started
			if(!started) {
				diff = begin-end;
				std::cout << "start it first... here have some non-sense:";
			}
			// Actually ended the measurement
			if(!running)
				diff = end-begin;
			else {
			// Still running
				std::cout << "measurement still going on!\n";
				diff = std::chrono::system_clock::now()-begin;
			}

			return diff.count();
		};
	private:
		bool started = false;
		bool running = false;
		std::chrono::duration<double> diff;
		std::chrono::time_point<std::chrono::system_clock> begin;
		std::chrono::time_point<std::chrono::system_clock> end;
};