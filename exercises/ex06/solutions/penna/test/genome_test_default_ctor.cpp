#include <genome.hpp>
#include <iostream>
#include <stdexcept>

using namespace Penna;

void test_default_constructor() {
    Genome genome;

    if (genome.count_bad(Genome::number_of_genes-1) != 0)
      throw std::logic_error("Default initialized Genome is not all zero.");
}

int main(int agrc, char** argv)
{

    // run tests
    test_default_constructor();

    return 0;
}
