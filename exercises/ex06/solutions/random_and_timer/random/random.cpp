
#include "random.hpp"

#include <limits>
#include <cassert>
#include <type_traits>

Generator::Generator(result_t seed) : seed_(seed) {
    assert(32 == std::numeric_limits<impl_t>::digits);
    assert(!std::is_signed<impl_t>::value);
}

Generator::result_t Generator::max() const {
  return std::numeric_limits<impl_t>::max();
}

    
Generator::result_t Generator::generate() {
  return seed_ = a_ * seed_ + c_;
}
