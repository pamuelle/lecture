#include <iostream>
#include <stdexcept>


class Plane {
public:
    void start() {
        std::cout << "Plane started successfully." << std::endl;
    }

    void serve_food() {
        throw std::runtime_error("The food is not edible!");
    }

    void land() {
        std::cout << "Plane landed successfully." << std::endl;
    }
};

int main() {
    // TODO: Modify this code so that the plane lands, but serve_food is still called.
    Plane plane;
    plane.start();
    try {
        plane.serve_food();
    }
    catch (std::runtime_error& e) {
        std::cerr << "Error: " << e.what() << "\n";
        std::cout << "Knocked out the passengers so they don't complain.\n";
    }
    plane.land();
}
