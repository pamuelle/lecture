import copy
import numpy as np
import random


# Store the genes as a numpy.array(dtype=bool).
gene_size = 64
# Genes set to all good.
genes_1 = np.zeros(gene_size, dtype=bool)

# Access individual gene with [ ].
print(genes_1[0])

# Deep copy genes_1.
genes_2 = copy.deepcopy(genes_1)


# Random numbers
# Seed the RNG.
random.seed(0)

# Random floating point number in the range [0.0, 1.0).
print(random.random())

# Random integer in the range [0, gene_size-1].
print(random.randint(0, gene_size - 1))
