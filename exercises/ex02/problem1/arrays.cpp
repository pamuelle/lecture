#include <iostream>
#include <vector>

void staticArray() {
	int n;
	double sum = 0.0;

	// Read n
	std::cout << "n=";
	std::cin >> n;

	double arr[n];

	

	std::cout << "Provide " << n << " values\n";

	// Read values
	for(unsigned i=0; i<n; ++i) {
		std::cin >> arr[i];
		sum += arr[i];
	}

	std::cout << "-----\n";

	// Normalize values and output it
	for(unsigned i=n; i>0; --i) {
		arr[i-1] /= sum;
		std::cout << arr[i-1] << "\n";
	}
}

void dynamicArray() {
	std::vector<double> vec;
	
	std::cout << "provide first value:\n";

	double foo;
	double sum = 0.0;
	while(std::cin >> foo && foo != 'exit') {
		vec.push_back(foo);
		sum += foo;
		std::cout << "next value \n";
	}

	for(unsigned i = 0; i<vec.size(); ++i) {
		std::cout << vec[i]/sum << "\n";
	}
}

int main() {
	bool useDynamicArray = false;

	std::cout << "0: static array\n 1: dynamic array\n";
	std::cin >> useDynamicArray;

	if(useDynamicArray) {
		dynamicArray();
	} else {
		staticArray();
	}

	return 0;
}