#include <iostream>
#include <cmath>
#include "simpson.hpp"

double f(double x) {
	return sqrt(x);
	//return x*(1-x);
}

int main() {
	int N=100000;

	// We split it for N=1 and N>1 to get nice
	// steps of size 1000
	// N=1
	//std::cout << 1 << "\t";
	//std::cout << simpson(&f,0,1,1) << "\n";

	// N>1
	for(unsigned i=1000; i<=N; i += 1000) {
		std::cout << i << "\t";
		std::cout << simpson(&f,0,1,i) << "\n";
	}

	return 0;
}