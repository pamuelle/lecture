# QUESTION: So I first used an old version of
#			libintegrate.a that printed stuff.
#			Now I recompiled it and make didn't
#			recognize the change in libintegrate.a
#			Does make only check diff of .ccp files?
#			What files does it check?
#
# QUESTION: What's the use of a static library
#			here eatly? Would we compile it in
#			the make as well?

.PHONY: all

all: simpson.out

simpson.o: simpson.cpp simpson.hpp
	${CXX} ${CXXFLAGS} -c $<

main.o: main.cpp simpson.hpp
	${CXX} ${CXXFLAGS} -c $<

simpson.out: main.o simpson.o
	${CXX} ${CXXFLAGS} -o $@ $^
	./simpson.out > result.txt
	./plot.sh

.PHONY: clean

clean:
	${RM} -v *.{o,out,txt,png}

