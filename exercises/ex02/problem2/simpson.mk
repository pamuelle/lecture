# QUESTION: "all: simpson.out clean" is that the
#			correct way to call clean after compilation?
#			NOTE: for the question, look at older commits
#				  was a stupid idea since I also deleted
#				  the "executable"...
#
# QUESTION: so why is .a part of gitignore?
#			should the makfile always create .a files?
#			if so, what's the point?



.PHONY: all

all: simpson.out

main.o: main.cpp simpson.hpp
	${CXX} ${CXXFLAGS} -c $<

simpson.out: main.o
	${CXX} ${CXXFLAGS} -o $@ $< -L. -lintegrate

.PHONY: clean

clean:
	${RM} -v *.{o,out}

