/*	NOTE 1:
		Using formula from https://en.wikipedia.org/wiki/Simpson%27s_rule
		-> int_a^b P(x) dx = frac{b-a}{6}(f(a)+4f(\frac{a+b}{2})+f(b))
	
	QUESTION: Where would I put Pre,POST,Note? In the cpp or hpp file?

	PRE: Integrates the integrand f on [a,b] using N bins
	POST: returns the value of int_a^b f dx using N bins
*/

double simpson(double (*f)(double), double a, double b, int N);