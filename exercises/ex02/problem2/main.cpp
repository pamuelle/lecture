#include <iostream>
#include <cmath>
#include "simpson.hpp"

double f(double x) {
	return sin(x);
	//return x*(1-x);
}

int main() {
	int N;
	std::cout << "N=";
	std::cin >> N;
	std::cout << simpson(&f, 0,1,N) << "\n\n";
	return 0;
}