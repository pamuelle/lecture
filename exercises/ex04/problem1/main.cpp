#include <iostream>

// Represent Z2 using a enum
enum Z2 { Plus, Minus };

Z2 operator*(Z2 a, Z2 b) {
	// Case -*- and +*+
	if((a == Minus && b == Minus) || (a == Plus && b == Plus)) {
		return Plus;
	}

	// Case +*- and -*+
	return Minus;
}

//
// Note: I'm not sure if I understand the exercise
//		 correctly for the next two overloads.
//		 I'm assuming T is an number althought it
//		 could be any type here.
//

// Assuming T is a number
template<class T>
T operator*(T a, Z2 b) {
	if(b == Minus) {
		return a*(-1);
	}

	return a;
}

// Assuming T is a number
template<class T>
T operator*(Z2 a, T b) {
	if(a == Minus) {
		return b*(-1);
	}

	return b;
}

// Overload << operator
std::ostream& operator<<(std::ostream& os, Z2 a) {
	if(a == Minus) {
		os << "Minus";
		return os;
	}

	os << "Plus";
	return os;
}

//
// Note: I don't get the hint. The whole exersice
//		 isn't written very well.
//

template<class T>
T mypow(T a, unsigned int n) {
	T tmp = a;

	while(n > 1) {
		tmp = tmp*a;
		--n;
	}

	return tmp;
}

int main() {
	Z2 a = Minus;

	int n = 134;

	std::cout << mypow(a, n) << "\n";;

	return 0;
}