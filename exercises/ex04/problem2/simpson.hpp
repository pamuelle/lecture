// QUESTION: Why exactly do I have to put the definition into the header file now?

#include <cmath>
#include <iostream>
#include <cassert>

class MyFunc {
public:
	double operator() (double x) {
		return sin(x);
	}
};

/*	NOTE 1:
		Using formula from https://en.wikipedia.org/wiki/Simpson%27s_rule
		-> int_a^b P(x) dx = frac{b-a}{6}(f(a)+4f(\frac{a+b}{2})
	PRE: Integrates the integrand f on [a,b] using N bins
	POST: returns the value of int_a^b f dx using N bins
*/
template<class T>
double simpson(MyFunc* f, T a, T b, int N) {
	assert(N>=1);
	assert(f!=NULL);
	
	double value = 0.0;
	double value_dx = 0.0;
	double delta_x = (double)(b-a)/N;

	std::cout << "\nIntegrating int_" << a << "^" << b
			  << "f(x)dx for N=" << N << "\n\n";

	for(double x = a; x <= b; x+=delta_x) {
		value += (delta_x/6.0)*( (*f)(x) + 4*(*f)(x+0.5*delta_x) + (*f)(x+delta_x) );
	}

	return value;
}