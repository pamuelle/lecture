#include <iostream>
#include "simpson.hpp"

int main() {
	int N;
	std::cout << "N=";
	std::cin >> N;

	MyFunc* f = new MyFunc();

	std::cout << simpson<int>(f, 0,1,N) << "\n\n";

	return 0;
}