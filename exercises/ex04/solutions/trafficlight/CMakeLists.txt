cmake_minimum_required(VERSION 2.8)

project(trafficlight)

add_executable(trafficlight trafficlight.cpp)
