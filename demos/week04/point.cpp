#include <cmath>
#include <iostream>
#include "point.hpp"

// constructors
Point::Point(double x, double y) : x_(x),y_(y) {}

// further properties & operations
// get x coordinate
double Point::x() const {
  return x_;
}
// get y coordinate
double Point::y() const {
  return y_;
}
// distance from origin aka polar radius
double Point::abs() const {
  return std::sqrt(x_*x_ + y_*y_);
}
// distance of this point to another point
double Point::dist(const Point& p) const {
  double xdiff = x_ - p.x_;
  double ydiff = y_ - p.y_;
  return std::sqrt(xdiff*xdiff + ydiff*ydiff);
}
// polar angle
double Point::angle() const {
  return std::atan2(y_,x_);
}
// mirror a point at origin (0,0)
void Point::invert() {
  x_ = - x_;
  y_ = - y_;
}

