#include <iostream>

class C {
  public:
    C(int i = 0) : i_(i) {
      std::cout << "ctor called\n";
    }
    ~C() {
      std::cout << "dtor called\n";
    }
    C(C const& c) : i_(c.i_) {
      std::cout << "copy ctor called\n";
    }
    C& operator=(C const& c) {
      std::cout << "copy assignment called\n";
      i_ = c.i_;
      return *this;
    }
  private:
    int i_;
};

int main() {

  C c1;
  C c2(c1);
  C c3{c2};
  C c4 = c3;
  c1 = c2 = c3 = c4;

}

