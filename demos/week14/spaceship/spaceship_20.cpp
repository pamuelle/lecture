// C++20
struct String20 {
	static constexpr int N = 20;
	char s[N];

	auto operator<=>(String20 const & other) const noexcept {
		for (int i = 0; i < N; ++i) {
			if (s[i] < other.s[i]) {
				return -1;
			} else if (s[i] > other.s[i]) {
				return +1;
			}
		}
		return 0;
	}
};
