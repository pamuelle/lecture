#include <cmath>
#include <iostream>
#include "simpson_17.hpp"

int main() {
	std::cout << integrate([](double x) { return std::sin(x); }, 0, 1, 100) << '\n';
}
