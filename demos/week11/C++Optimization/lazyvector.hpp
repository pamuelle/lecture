#include <algorithm> // for std::swap
#include <cassert>

// forward-declare vectorsum class as it is needed in lazyvector class
template <typename T>
class vectorsum;

// lazyvector
template <typename T>
class lazyvector {
  public:
    typedef T value_type;
    typedef T& reference;
    typedef unsigned int size_type;
    // ctor
    lazyvector(size_type s=0) : p_(new value_type[s]),sz_(s) {}
    // copy ctor
    lazyvector(const lazyvector& v) : p_(new value_type[v.size()])
                                    , sz_(v.size()) {
      for (int i=0;i<size();++i) {
        p_[i] = v.p_[i];
      }
    }
    // dtor
    ~lazyvector() { delete[] p_;}
    // swap
    void swap(lazyvector& v) {
      std::swap(p_,v.p_);
      std::swap(sz_,v.sz_);
    }
    // copy assignment
    lazyvector& operator=(lazyvector v) {
      swap(v);
      return *this;
    }
    // assignment from a vectorsum object
    lazyvector& operator=(const vectorsum<T>& v) {
      for (int i=0; i<size(); ++i)
        p_[i] = v[i];
      return *this;
    }
    // size
    size_type size() const { return sz_; }
    // subscript operator
    value_type operator[](size_type i) const { return p_[i]; }
    reference operator[](size_type i) { return p_[i]; }

  private:
    value_type* p_;
    size_type sz_;
};

// vectorsum class
template <typename T>
class vectorsum {
  public:
    typedef T value_type;
    typedef unsigned int size_type;
    // ctor
    vectorsum(const lazyvector<T>& x,const lazyvector<T>& y) : left_(x)
                                                             , right_(y) {}
    // subscript operator
    value_type operator[](size_type i) const {
      return left_[i] + right_[i];
    }

  private:
    const lazyvector<T>& left_;
    const lazyvector<T>& right_;
};

// binary "+" operator (non-member / free function)
template <typename T>
inline vectorsum<T> operator+(const lazyvector<T>& x,const lazyvector<T>& y) {
  return vectorsum<T>(x,y);
}

