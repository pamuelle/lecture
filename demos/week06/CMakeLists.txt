cmake_minimum_required(VERSION 2.8)

# C++11
set(CMAKE_CXX_STANDARD 11)

# setting warning compiler flags
if(CMAKE_CXX_COMPILER_ID MATCHES "(C|c?)lang")
  add_compile_options(-Weverything)
else()
  add_compile_options(-Wall -Wextra -Wpedantic)
endif()

add_executable(exceptional exceptional.cpp)
add_executable(hello_date hello_date.cpp)
add_executable(fibo fibo.cpp)
add_executable(timed_loop timed_loop.cpp)
add_executable(random_engines random_engines.cpp)
add_executable(random_normal random_normal.cpp)

