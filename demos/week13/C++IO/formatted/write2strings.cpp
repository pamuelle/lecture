#include <iostream>
#include <iomanip>
#include <sstream>

int main() {

  std::ostringstream ss;
  ss << std::setw(5) << std::setfill('0') << 42;
  std::string s2(ss.str());
  std::cout << s2 << '\n';

}

