	.file	"loopadd.cpp"
	.text
	.p2align 4
	.globl	_Z3sumPdPKdS1_
	.type	_Z3sumPdPKdS1_, @function
_Z3sumPdPKdS1_:
.LFB0:
	.cfi_startproc
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L2:
	movsd	(%rsi,%rax), %xmm0
	addsd	(%rdx,%rax), %xmm0
	movsd	%xmm0, (%rdi,%rax)
	addq	$8, %rax
	cmpq	$81920, %rax
	jne	.L2
	ret
	.cfi_endproc
.LFE0:
	.size	_Z3sumPdPKdS1_, .-_Z3sumPdPKdS1_
	.ident	"GCC: (GNU) 9.2.0"
	.section	.note.GNU-stack,"",@progbits
