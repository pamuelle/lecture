from mpi4py import MPI

# To run this on Euler, load the following modules
#
#   module load open_mpi python
#
# and submit it to the batch system, e.g.
#
#   bsub -n 10 mpirun python mpi_hello.py
#

# get COMMON WORLD communicator, size & rank
comm    = MPI.COMM_WORLD
size    = comm.Get_size()
my_rank = comm.Get_rank()

# hello!
msg = "Hello world from processor with rank {:2d} ".format(my_rank) \
    + "out of {:2d} processors".format(size)
print(msg)

